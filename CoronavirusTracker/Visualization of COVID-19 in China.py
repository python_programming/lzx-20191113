from pyecharts import options as opts  # 地图包
from pyecharts.charts import Map  # 地图包
from pyecharts.globals import ThemeType  # 柱状图包
from pyecharts.charts import Bar  # 柱状图包
from pyecharts.charts import Line  # 渐变折线图包
from pyecharts.commons.utils import JsCode  # 渐变折线图包
from pyecharts.charts import WordCloud  # 词云图包
import requests, time, re, json, os
import pandas as pd

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.90 Safari/537.36',
    'Referer': 'https://news.sina.cn/zt_d/yiqing0121'
}

def get_html(url):
    html = requests.get(url, headers=headers)
    take_json = re.compile('{"data_title":"fymap","data":(.*?)}}')  # 用正则表达式取中间

    new_json = take_json.findall(html.text)
    new_json = json.loads(new_json[0] + '}}')

    province_dict = {
        '省份名字': [],
        '省份英文名字': [],
        '目前确诊人数': [],
        '累计确诊人数': [],
        '治愈人数': [],
        '目前死亡人数': [],
        '现存无症状人数': [],
        '目前怀疑感染人数': [],
        '目前境外输入人数': [],
        '较昨日增加人数': [],
        '连续无感染天数': [],
    }

    city_dict = {
        '城市名字': [],
        '目前确诊人数': [],
        '累计确诊人数': [],
        '目前治愈人数': [],
        '目前死亡人数': [],
        '现存无症状人数': [],
        '目前怀疑感染人数': [],
        '目前境外输入人数': [],
        '较昨日增加人数': [],
        '连续无感染天数': [],
    }

    for i in new_json['list']:
        province_dict['省份名字'].append(i['name'])  # 城市名字
        province_dict['省份英文名字'].append(i['ename'])  # 城市英文名字
        province_dict['现存无症状人数'].append(i['asymptomNum'])  # 现存无症状人数
        province_dict['治愈人数'].append(i['cureNum'])  # 治愈人数
        province_dict['目前死亡人数'].append(i['deathNum'])  # 目前死亡人数
        province_dict['目前确诊人数'].append(i['econNum'])  # 目前确诊人数
        province_dict['累计确诊人数'].append(i['value'])  # 累计确诊人数
        province_dict['目前怀疑感染人数'].append(i['susNum'])  # 目前怀疑感染人数
        province_dict['目前境外输入人数'].append(i['jwsrNum'])  # 目前境外输入人数
        province_dict['较昨日增加人数'].append(i['conadd'])  # 较昨日增加人数
        province_dict['连续无感染天数'].append(i['zerodays'])  # 连续无感染天数

        for t in i['city']:
            city_dict['城市名字'].append(t['mapName'])  # 城市名字
            city_dict['累计确诊人数'].append(t['conNum'])  # 城市累计确诊人数
            city_dict['目前治愈人数'].append(t['cureNum'])  # 城市治愈人数
            city_dict['目前死亡人数'].append(t['deathNum'])  # 城市治愈人数
            city_dict['目前确诊人数'].append(t['econNum'])  # 目前确诊人数
            city_dict['较昨日增加人数'].append(t['conadd'])  # 较昨日增加人数
            city_dict['连续无感染天数'].append(t['zerodays'])  # 城连续无感染天数
            city_dict['目前境外输入人数'].append(t['jwsr'])  # 目前境外输入人数
            city_dict['现存无症状人数'].append(t['asymptomNum'])  # 现存无症状人数
            city_dict['目前怀疑感染人数'].append(t['susNum'])  # 目前怀疑感染人数

    province_data = pd.DataFrame(province_dict)
    city_data = pd.DataFrame(city_dict)

    for i in province_dict:  # 迭代数据  如果出现空数据则补0
        province_data[i] = province_data[i].apply(lambda x: '0' if x == '' else x)

    for i in city_dict:  # 迭代数据  如果出现空数据则补0
        city_data[i] = city_data[i].apply(lambda x: '0' if x == '' else x)

    city_data = city_data[~city_data['城市名字'].isin(['境外', '外来', '0'])]  # 取反 筛选掉没用数据

    province_id = province_data['省份名字'].values.tolist()  # .values.tolist()为转换为列表形式
    province_set = province_data['累计确诊人数'].values.tolist()

    filename = 'Visualization in China'
    if not os.path.exists(filename):  # 生成文件夹
        os.makedirs(filename)

    di_tu(province_id, province_set)  # 生成地图
    zhu_zhuang(province_id, province_set)  # 生成柱状图

    city_id = city_data['城市名字'].values.tolist()
    city_set = city_data['累计确诊人数'].values.tolist()

    whole_id = province_id + city_id
    whole_data = province_set + city_set

    ci_yun(whole_id, whole_data)  # 生成词云图

    province_set = province_data['治愈人数'].values.tolist()
    zhe_xian(province_id, province_set)  # 生成折线图

    province_data.to_csv("中国疫情省份分析.csv", encoding='utf-8-sig')
    city_data.to_csv("中国疫情城市分析.csv", encoding='utf-8-sig')





def di_tu(your_id, your_data):  # 地图  our_id为名字  your_id为数据
    m = Map(opts.InitOpts(width='1260px',height='600px',page_title="中国新型冠状肺炎疫情分布地图"))
    m.add("确诊人数", [list(z) for z in zip(your_id, your_data)], "china")
    m.set_global_opts(title_opts=opts.TitleOpts(title="中国新型冠状肺炎疫情分布地图"),
                      visualmap_opts=opts.VisualMapOpts(max_=800))
    m.render("./Visualization in China/Map.html")


def zhu_zhuang(your_id, your_data):  # 柱状图  your_id为名字  your_id为数据

    bar = Bar(
        init_opts=opts.InitOpts(
            theme=ThemeType.PURPLE_PASSION,
            width="1260px",
            height="600px",
        page_title="中国新型冠状肺炎疫情情况柱状图"))
    bar.add_xaxis(your_id)
    bar.add_yaxis('确诊人数', your_data)
    bar.set_global_opts(
        title_opts=opts.TitleOpts(
            title="中国新型冠状肺炎疫情情况柱状图"),
        datazoom_opts=[opts.DataZoomOpts()],  # ------窗口滑块
        xaxis_opts=opts.AxisOpts(axislabel_opts=opts.LabelOpts(rotate=10)))  # 旋转标签
    bar.render("./Visualization in China/Histogram.html")


def ci_yun(your_id, your_data):  # 词云图  your_id为名字  your_id为数据

    new = list(zip(your_id, your_data))

    words = new

    wordcloud = WordCloud(init_opts=opts.InitOpts(width="1260px", height='600px', page_title="中国新型冠状肺炎疫情情况词云图"))
    wordcloud.add('', words, word_size_range=[20, 100])
    wordcloud.set_global_opts(title_opts=opts.TitleOpts(title="中国新型冠状肺炎疫情情况词云图"))

    wordcloud.render('./Visualization in China/WordCloud.html')


def zhe_xian(your_id, your_data):  # 折线图  your_id为名字  your_id为数据

    x_data = your_id
    y_data = your_data

    background_color_js = (
        "new echarts.graphic.LinearGradient(0, 0, 0, 1, "
        "[{offset: 0, color: '#c86589'}, {offset: 1, color: '#06a7ff'}], false)"
    )
    area_color_js = (
        "new echarts.graphic.LinearGradient(0, 0, 0, 1, "
        "[{offset: 0, color: '#eb64fb'}, {offset: 1, color: '#3fbbff0d'}], false)"
    )

    c = (
        Line(init_opts=opts.InitOpts(bg_color=JsCode(background_color_js),width='1260px',height='600px',page_title="中国新型冠状肺炎疫情情况折线图"))
            .add_xaxis(xaxis_data=x_data)
            .add_yaxis(
            series_name="治愈人数总量",
            y_axis=y_data,
            is_smooth=True,
            is_symbol_show=True,
            symbol="circle",
            symbol_size=6,
            linestyle_opts=opts.LineStyleOpts(color="#fff"),
            label_opts=opts.LabelOpts(is_show=True, position="top", color="white"),
            itemstyle_opts=opts.ItemStyleOpts(
                color="red", border_color="#fff", border_width=3
            ),
            tooltip_opts=opts.TooltipOpts(is_show=False),
            areastyle_opts=opts.AreaStyleOpts(color=JsCode(area_color_js), opacity=1),
        )
            .set_global_opts(
            title_opts=opts.TitleOpts(
                title="目前治愈人数",
                pos_bottom="5%",
                pos_left="center",
                title_textstyle_opts=opts.TextStyleOpts(color="#fff", font_size=16),
            ),
            xaxis_opts=opts.AxisOpts(
                type_="category",
                boundary_gap=False,
                axislabel_opts=opts.LabelOpts(margin=30, color="#ffffff63",interval=0,rotate=30),
                axisline_opts=opts.AxisLineOpts(is_show=False),
                axistick_opts=opts.AxisTickOpts(
                    is_show=True,
                    length=25,
                    linestyle_opts=opts.LineStyleOpts(color="#ffffff1f"),
                ),
                splitline_opts=opts.SplitLineOpts(
                    is_show=True, linestyle_opts=opts.LineStyleOpts(color="#ffffff1f")
                ),
            ),
            yaxis_opts=opts.AxisOpts(
                type_="value",
                position="right",
                axislabel_opts=opts.LabelOpts(margin=20, color="#ffffff63"),
                axisline_opts=opts.AxisLineOpts(
                    linestyle_opts=opts.LineStyleOpts(width=2, color="#fff")
                ),
                axistick_opts=opts.AxisTickOpts(
                    is_show=True,
                    length=15,
                    linestyle_opts=opts.LineStyleOpts(color="#ffffff1f"),
                ),
                splitline_opts=opts.SplitLineOpts(
                    is_show=True, linestyle_opts=opts.LineStyleOpts(color="#ffffff1f")
                ),
            ),
            legend_opts=opts.LegendOpts(is_show=False),
        )
    )
    c.render('./Visualization in China/LineChart.html')


if __name__ == '__main__':
    new_time = int(time.time() * 1000)
    url = 'https://gwpre.sina.cn/interface/fymap2020_data.json?_={}&callback=dataAPIData'.format(new_time)
    get_html(url)