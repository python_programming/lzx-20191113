# import urllib3
# http = urllib3.PoolManager()
# response = http.request('GET','http://www.besti.edu.cn/')
# print(response.data.decode())
#https://www.whatismyip.com/
import requests
# # 网络超时
# try:
#     response = requests.get('http://www.besti.edu.cn/',timeout = 0.7)
#     print(response.status_code)
# except Exception as e:
#     print('异常',str(e))

# 头部请求
# for item in response.headers:
#     print(item,':',response.headers[item])

from bs4 import BeautifulSoup
response = requests.get('https://www.whatismyip.com/')
soup = BeautifulSoup(response.text,features='lxml')
print(soup.find('title').text)