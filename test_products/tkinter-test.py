# -*- coding: UTF-8 -*-
import tkinter
from tkinter import messagebox
import socket
# 文件传输主操作
def SendOut():
    client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    client.connect(('127.0.0.1',8001))
    with open(FilePath,'rb') as file:
        for i in file:
            client.send(i)
            data = client.recv(1024)
            if data != b'success':
                break
    client.send('quit'.encode())
# 弹窗
class MyCollectApp(tkinter.Toplevel):
    def __init__(self):
        super().__init__()
        self.title('文件信息')
        self.setupUI()

    def setupUI(self):
        row1 = tkinter.Frame(self)
        row1.pack(fill="x")
        l1 = tkinter.Label(row1, text="文件路径：", height=2, width=10)
        l1.pack(side=tkinter.LEFT)  # 这里的side可以赋值为LEFT  RTGHT TOP  BOTTOM
        self.xls_text = tkinter.StringVar()
        tkinter.Entry(row1, textvariable=self.xls_text).pack(side=tkinter.RIGHT)

        row2 = tkinter.Frame(self)
        row2.pack(fill="x")
        tkinter.Button(row2, text="点击确认", command=self.on_click).pack(side=tkinter.RIGHT)

    def on_click(self):
        # print(self.xls_text.get().lstrip())
        global FilePath
        FilePath = self.xls_text.get().lstrip()
        if len(FilePath) == 0:
            messagebox.showwarning(title='系统提示', message='请输入文件路径!')
            return False
        self.quit()
        self.destroy()
        SendOut()
        print("文件传输成功！")

if __name__ == '__main__':
    window = tkinter.messagebox.askyesno(title='系统提示', message='是否传输文件？')  # 返回'True','False'
    if window:
        app = MyCollectApp()
        app.mainloop()
    else:
        print('无操作实现')


# def TakeIn():
#     # 输入
#     t1 = tkinter.StringVar()
#     t1.set('春季里那个百花开')
#     entry = tkinter.Entry(window, textvariable=t1).pack()
#     print(t1.get())
#
#
# def SendOut():
#     pass
#
#
#
# window = tkinter.Tk()
#
# # 框架设置
# window.title('文件传输')
# window.geometry('400x200')
# w = tkinter.Label(window,text='请选择相关文档传输操作')
# w.pack(side=tkinter.TOP)
#
# # # 输入
# # t1 = tkinter.StringVar()
# # t1.set('春季里那个百花开')
# # entry = tkinter.Entry(window, textvariable = t1).pack()
# # print (t1.get())
#
# # 选项按钮
# yes = tkinter.Button(window, text='传输',command = SendOut())
# no = tkinter.Button(window,text='接收',command = TakeIn())
# yes.pack()
# no.pack()
#
#
# window.mainloop()


# import tkinter as tk
# from tkinter import filedialog
#
# def upload_file():
#     selectFile = tk.filedialog.askopenfilename()  # askopenfilename 1次上传1个；askopenfilenames1次上传多个
#     entry1.insert(0, selectFile)
#
#
# root = tk.Tk()
#
# frm = tk.Frame(root)
# frm.grid(padx='20', pady='30')
# btn = tk.Button(frm, text='上传文件名', command=upload_file)
# btn.grid(row=0, column=0, ipadx='3', ipady='3', padx='10', pady='20')
# entry1 = tk.Entry(frm, width='40')
# entry1.grid(row=0, column=1)
#
# root.mainloop()