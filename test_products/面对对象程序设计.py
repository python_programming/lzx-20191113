# class Geese:
#     '''大雁类'''
#     neck = "脖子较长"  #类属性（脖子）
#     wing = "振翅频率高"  #类属性（翅膀）
#     leg = "腿位于身体的中心支点，行走自如"  #类属性（腿）
#     number = 0
#     def __init__(self):  #定义构造方法
#         Geese.number += 1
#         print("\n我是第%d只大雁：我属于大雁类，我有以下特征："%Geese.number)
#         print(Geese.neck)
#         print(Geese.wing)
#         print(Geese.leg)
#
# list = []
# for i in range(4):  #循环四次
#     list.append(Geese())  #创建大雁类的实例
# print("一共有%d只大雁！"%Geese.number)  #输出大雁只数
# Geese.beak = "喙的基部较高，长度和头部几乎相等"  #添加类属性
# print(list[2].beak)  #输出第三只大雁的喙特征
# print(Geese.beak)




class Geese:
    '''大雁类'''
    def __init__(self):  #定义构造方法
        self.neck = "脖子较长"  #实例属性（脖子）
        self.wing = "振翅频率高"  #实例属性（翅膀）
        self.leg = "腿位于身体的中心支点，行走自如"  #实例属性（腿）
        print("\n我属于大雁类，我有以下特征：")
        print(self.neck)
        print(self.wing)
        print(self.leg)
geese = Geese()  #实例化类的对象
geese_1 = Geese()
geese_1.leg = "呵呵呵"
#print(Geese.leg)
print(geese.leg)
print(geese_1.leg)

# class Swan:
#     '''天鹅类'''
#     __neck_swan = "天鹅的脖子很长"  #私有类属性
#     def __init__(self):
#         print("__init__():",Swan.__neck_swan)  #访问私有类型的属性
#     # def my(self):
#     #     print("my方法：",Swan.__neck_swan)  #访问私有类型的属性    my方法并没有被使用——只使用一个方法
# swan = Swan()  #创建Swan类的实例（对象）
#
# #print("直接访问：",swan._neck_swan)  无法直接访问
# print("直接访问：",swan._Swan__neck_swan)
# swan._Swan__neck_swan = "脖子很长"  #修改私有类型的属性
# print("直接访问：",swan._Swan__neck_swan)  #访问私有类型的属性

# class Rect:
# #     def __init__(self,width,height):  #构造方法
# #         self.width = width  #矩形的宽度
# #         self.height = height  #矩形的高度
# #     @property
# #     def area(self):
# #         return self.width*self.height  #计算矩形面积
# # rect = Rect(800,600)  #创建类的实例
# # print("面积为：",rect.area)  #输出面积
# # # rect.area = 100
# # # print("面积为：",rect.area)

# class TVShow:  #电视节目类
#     film = ['战狼2','红海行动','西游记女儿国']
#     def __init__(self,show):
#         self.__show = show
#     @property
#     def show(self):
#         return self.__show  #返回私有属性
#     @show.setter
#     def show(self,value):
#         if value in TVShow.film:  #判断值是否在列表中
#             self.__show = "您选择了《"+value+"》，稍后将播放"  #修改返回的值
#         else:
#             self.__show = "您点播的电影不存在"
#
# tvshow = TVShow("战狼2")  #创建类的实例
# print("正在播放：《"+tvshow.show+"》")  #获取属性值
# print("您可以从",TVShow.film,"中选择要点播的电影")
# tvshow.show = "红海行动"
# print(tvshow.show)  #获取属性值