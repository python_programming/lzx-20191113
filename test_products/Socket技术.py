# TCPClient
import socket
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
# 参数1：服务器之间的网络通信；参数2：流式Socket，for TCP
s.connect(('127.0.0.1',8001))
str = input("请输入要传输的内容：")
s.sendall(str.encode())
data = s.recv(1024)    # 1024使接收数据的大小，需要设置
print(data.encode())
s.close()

# TCPServer
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind(('127.0.0.1',8001))    # port = 0 ——65535
s.listen()
conn,address = s.accept()    # conn接受客户端发送的信息
data = conn.recv(1024)
print(data.encode())
conn.sendall(("服务器已经接收到了数据内容："+str(data)).encode())
s.close()

