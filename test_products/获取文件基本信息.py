import os
def formatTime(longtime):
    '''格式化时间'''
    import time
    return time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())
def formatByte(number):
    '''格式化文件大小的函数'''
    for (scale,label) in [(1024*1024*1024,'GB'),(1024*1024,'MB'),(1024,'KB')]:
        if number >= scale:
            return '%.2f %s' % (number*1.0/scale,label)
        elif number == 1:
            return "1字节"
        else:
            byte = '%.2f' % (number or 0)
    return (byte[:-3] if byte.endswith('.00') else byte)+'字节'

fileinfo = os.stat(r"F:\作业\社会心理学\研究项目题目+研究计划书.doc")
print("文件完整路径：",os.path.abspath(r"F:\作业\社会心理学\研究项目题目+研究计划书.doc"))
print("索引号：",fileinfo.st_ino)
print("设备名：",fileinfo.st_dev)
print("文件大小：",formatByte(fileinfo.st_size))
print("最后一次访问时间：",formatTime(fileinfo.st_atime))