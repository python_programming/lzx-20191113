
# def fac(n):
#     t = 1
#     for i in range(1,n+1):
#         t = t * i
#     return t
#
# n = int(input())
# r = fac(n)
# print(r)

#import math
# def qua(a,b,c):     # 解一元二次方程
#     d = b*b - 4*a*c
#     if d>=0:
#         x1 = (-b+math.sqrt(d)) / (2*a)
#         x2 = (-b-math.sqrt(d)) / (2*a)
#         if d==0:
#             return "有唯一的解：X = "+str(x1)
#         else:
#             return "X1 = "+str(x1)+'\t'+"X2 = "+str(x2)
#     else:
#         return "在实数范围内无解"
# a = float(input("a = "))
# b = float(input("b = "))
# c = float(input("c = "))
# print(qua(a,b,c))

#!/usr/bin/env python3.5
# -*-coding:utf8-*-
'''import re
a =r'1 - 2 * ( (60-30 +(-40/5) * (9-2*5/3 + 7 /3*99/4*2998 +10 * 568/14 ))- (-4*3)/(16-3*2))'
# ＊／运算函数
def mul_div(str):
  calc = re.split("[*/]",str)   #用＊/分割公式
  OP = re.findall("[*/]",str)  #找出所有＊和／号
  ret = None
  for index,i in enumerate(calc):
    if ret:
      if OP[index-1] == "*":
        ret *= float(i)
      elif OP[index-1] == "/":
        ret /= float(i)
    else:
      ret = float(i)
  return ret

# 去掉重复运算,和处理特列+-符号
def del_double(str):
  str = str.replace("++", "+")
  str = str.replace("--", "-")
  str = str.replace("+-","-")
  str = str.replace("- -","-")
  str = str.replace("+ +","+")
  return str

# 计算主控制函数
def calc_contrl(str):
  tag = False
  str = str.strip("()") # 去掉最外面的括号
  str = del_double(str) # 调用函数处理重复运算
  find_ = re.findall("[+-]",str) # 获取所有+－ 操作符
  split_ = re.split("[+-]",str) #正则处理 以+－操作符进行分割，分割后 只剩*/运算符
  if len(split_[0].strip()) == 0: # 特殊处理
    split_[1] = find_[0] + split_[1] # 处理第一个数字前有“－”的情况，得到新的带符号的数字
    # 处理第一个数字前为负数“-"，时的情况，可能后面的操作符为“－”则进行标记
    if len(split_) == 3 and len(find_) ==2:
      tag =True
      del split_[0] # 删除原分割数字
      del find_[0]
    else:
      del split_[0] # 删除原分割数字
      del find_[0] # 删除原分割运算符
  for index, i in enumerate(split_):
    # 去除以*或/结尾的运算数字
    if i.endswith("* ") or i.endswith("/ "):
      split_[index] = split_[index] + find_[index] + split_[index+1]
      del split_[index+1]
      del find_[index]
  for index, i in enumerate(split_):
    if re.search("[*/]",i): # 先计算含*/的公式
      sub_res = mul_div(i) #调用剩除函数
      split_[index] = sub_res
  # 再计算加减
  res = None
  for index, i in enumerate(split_):
    if res:
      if find_[index-1] == "+":
        res += float(i)
      elif find_[index-1] == "-":
        # 如果是两个负数相减则将其相加，否则相减
        if tag == True:
          res += float(i)
        else:
          res -= float(i)
    else:
      # 处理没有括号时会出现i 为空的情况
      if i != "":
        res = float(i)
  return res

if __name__ == '__main__':
  calc_input = input("请输入计算公式，默认为:%s：" %a).strip()
  try:
    if len(calc_input) ==0:
      calc_input = a
    calc_input = r'%s'%calc_input # 做特殊处理，保持字符原形
    flag = True  # 初始化标志位
    result = None  # 初始化计算结果
    # 循环处理去括号
    while flag:
      inner = re.search("\([^()]*\)", calc_input)# 先获取最里层括号内的单一内容
      #print(inner.group())
      # 有括号时计算
      if inner:
        ret = calc_contrl(inner.group()) # 调用计算控制函数
        calc_input = calc_input.replace(inner.group(), str(ret)) # 将运算结果，替换原处理索引值处对应的字符串
        print("处理括号内的运算[%s]结果是：%s" % (inner.group(),str(ret)))
        #flag = True
      # 没有括号时计算
      else:
        ret = calc_contrl(calc_input)
        print("最终计算结果为：%s"% ret)
        #结束计算标志
        flag = False
  except:
    print("你输入的公式有误请重新输入！")'''


# print("1.二元加法运算\n2.二元减法运算\n3.二元乘法运算\n4.二元除法运算\n")
# temp = 233
# while temp!=1 and temp!=2 and temp!=3 and temp!=4:
#     temp = int(input("请依照所要使用的功能输入相应的数字序号："))
#     if temp==1 or temp==2 or temp==3:
#         x = float(input("请输入第一个操作数："))
#         y = float(input("请输入第二个操作数："))
#         if temp==1:
#             print(x+y)
#         elif temp==2:
#             print(x-y)
#         else:
#             print(x*y)
#     elif temp==4:
#         x = float(input("请输入被除数："))
#         y = float(input("请输入除数："))
#         while y == 0:
#             print("输入错误！除数不能为0！")
#             y = float(input("请重新输入除数："))
#         print(x / y)
#     else:
#         print("相应功能数字序号输入有误！")
#         print("1.二元加法运算\n2.二元减法运算\n3.二元乘法运算\n4.二元除法运算\n")

import math
x = float(input("请以角度制输入角度："))
x = x/180 * math.pi
print(math.sin(x))
x = float(input("请输入相应数值:"))
print('%.2f' % math.degrees(math.asin(x)))

import math
'''统计学运算'''
x = int(input("输入数据个数："))
y = []
print("请输入数据：")
for i in range(x):
    temp = [float(input())]
    y += temp
print("\n1.求和\n2.求平均值\n3.求极差\n4.求方差\n5.求标准差")
temp = input("请依照所要使用的功能输入相应的数字序号：")
s = 0
for i in range(y):
    s =s + y[i]
var = 0
for i in range(x):
    var = var+(s/x-y[i])**2
var = var / x
if temp=='1':
    print(s)
elif temp=='2':
    print(s/x)
elif temp=='3':
    print(max(y)-min(y))
elif temp=='4':
    print(var)
elif temp=='5':
    print(math.sqrt(var))

