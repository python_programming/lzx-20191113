# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：北京电子科技学院学生信息输入
时间：2020年4月8日
'''
# 封装
class School:
    '''学校信息'''
    def __init__(self):
        self.__school = "北京电子科技学院"
        self.__address = "富丰路7号"
        print("\n学校："+self.__school)
        print("地址："+self.__address)
# 继承
class Student(School):
    '''学生信息类'''
    def __init__(self,name,age):
        super(Student,self).__init__()
        Student.name = name
        Student.age = age
        Student.major = "信息与计算科学"
        print("专业："+Student.major)
        print("姓名："+Student.name+"\n年龄："+Student.age)
    # 定义类的方法
    def Reset(self,name,age):
        super(Student, self).__init__()
        Student.name = name
        Student.age = age
        print("专业：" + Student.major)
        print("姓名：" + Student.name + "\n年龄：" + Student.age)


name = input("请输入学生姓名：")
age = input("请输入学生年龄：")
student = Student(name,age)
print("\n是否需要需修改？")
print("需要请输入数字1，不需要请输入任意数字。")
t = input("请输入：")
if t=="1":
    name = input("请输入学生姓名：")
    age = input("请输入学生年龄：")
    student.Reset(name,age)
