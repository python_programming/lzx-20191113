# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：Python之Socket技术及安全编程技术——TCP客户端
时间：2020年5月6日
'''
import socket
# 创建套接字，连接远端地址
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect(('127.0.0.1',8001))
# 连接后发送数据和接收数据
str = input("请输入要传输的内容：")
s.sendall(str.encode())
data = s.recv(1024)
print(data.decode())
# 传输完毕后，关闭套接字
s.close()