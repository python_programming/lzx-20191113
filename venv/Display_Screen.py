'''
作者：林紫欣
文件名称：火车站显示屏
时间：2020年3月25日
'''
# 输出车票信息
template = "  {:^4}      {:^2}-{:^2}      {:0>5}      {:0>5}     {:0>5}"
trains = ["T40","T298","Z158","Z62"]
departure_stations = ["长春","长春","长春","长春"]
arrival_stations = ["北京","北京","北京","北京"]
departure_times = ["00:12","00:06","12:48","21:58"]
arrival_times = ["12:20","10:50","21:06","06:08"]
durations = ["12:08","10:44","08:18","8:20"]
print("  车次    出发站-到达站   出发时间   到达时间    历时")
x = 4
for i in range(x):
    print(template.format(trains[i].title(),departure_stations[i],arrival_stations[i],departure_times[i],arrival_times[i],durations[i]))

#更新、修改车票信息
print("\n\n是否需要更新信息？\n如需添加班次信息请输入数字1\n如需修改信息请输入数字2\n不需要更新请输入数字0")
t = int(input("\n请输入相关数字："))
while t == 1 or t == 2:
    if t == 1:
        x += 1
        train = input("请输入车次：")
        trains.append(train)
        departure_station = input("请输入出发站：")
        departure_stations.append(departure_station)
        arrival_station = input("请输入到达站：")
        arrival_stations.append(arrival_station)
        departure_time = input("请输入出发时间（例如13:14）：")
        departure_times.append(departure_time)
        arrival_time = input("请输入到达时间（例如13:14）：")
        arrival_times.append(arrival_time)
        duration = input("请输入历经时长（例如13:14）：")
        durations.append(duration)
    elif t == 2:
        temp = input("请输入要修改的班次：")
        for i in range(x):
            if  temp.title() == trains[i]:
                while t == 2:
                    information = input("请输入要修改的项目（车次、出发站、到达站、出发时间、到达时间、历时，仅限一个）：")
                    if information == "车次":
                        trains[i] = input("请输入修改后的车次：")
                    elif information == "出发站":
                        departure_stations[i] = input("请输入修改后的出发站：")
                    elif information == "到达站":
                        arrival_stations[i] = input("请输入修改后的到达站：")
                    elif information == "出发时间":
                        departure_times[i] = input("请输入修改后的出发时间（例如13:14）：")
                    elif information == "到达时间":
                        arrival_times[i] = input("请输入修改后的到达时间（例如13:14）：")
                    elif information == "历时":
                        durations[i] = input("请输入修改后的总时长（例如13:14）：")
                    print("\n%s班次是否还有其他信息需要修改？\n如果需要的话请输入数字2\n不需要的话请输入数字0"%(trains[i].title()))
                    t = input("\n请输入相关数字：")

    print("\n是否需要再次更新信息？\n如需添加班次信息请输入数字1\n如需修改信息请输入数字2\n不需要更新请输入除1以外的任意数字")
    t = int(input("\n请输入相关数字："))

print("\n请核查信息：\n")
print("  车次    出发站-到达站   出发时间   到达时间    历时")
for i in range(x):
    print(template.format(trains[i].title(),departure_stations[i],arrival_stations[i],departure_times[i],arrival_times[i],durations[i]))