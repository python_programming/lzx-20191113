# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：TCP通信——服务端
时间：2020年5月17日
'''

import socket
import  threading
import  time
import base64
from Crypto.Cipher import AES

SerSock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
SerSock.bind(('127.0.0.1',8001))
# SerSock.close()
SerSock.listen(5)
socks = []  # 存放每个客户端的socket


def take_in(filename,s):
    while True:
        with open(filename, 'ab') as file:
            data = s.recv(1024)
            if data == b'quit':
                break
            file.write(data)
        time.sleep(1)
        s.sendall('success'.encode())
    print("... File reception completed ...")

def add_to_16(value):# str不是16的倍数那就补足为16的倍数
    while len(value) % 16 != 0:
        value += '\0'
    return str.encode(value)  # 返回bytes

def encrypt(filename,af_filename):#加密方法
    key = input("Please set a key:")
    text = open(filename,'rb').read()
    open(filename,'rb').close()
    text = str(text)
    aes = AES.new(add_to_16(key), AES.MODE_ECB)
    encrypt_aes = aes.encrypt(add_to_16(text))
    encrypted_text = str(base64.encodebytes(encrypt_aes), encoding='utf-8')
    logbat = open(af_filename, 'w')
    logbat.write(encrypted_text)
    logbat.close()
    print('... File encrypted successfully ...')

def send_out(filename,s):
    with open(filename, 'rb') as file:
        i = file.read()
        s.send(i)
    time.sleep(1)
    s.send('quit'.encode())
    print("... File sent successfully ...")

def handle():
    while True:
        for s in socks:
            try:
                data = s.recv(1024).decode('utf-8')
                if data[0:len('encrypt file')] == 'encrypt file':
                    print("Enter file encryption ...")
                    filename = 'Server.txt'
                    af_filename = 'Server encrypted.txt'
                    take_in(filename,s)
                    encrypt(filename, af_filename)
                    send_out(af_filename,s)
                elif data == 'quit':
                    print('\ncommunication ended.')
                    break
                else:
                    print("\nClient is sending:",data)
                    re = input("reply:")
                    s.send(re.encode('utf-8'))
            except Exception:
                continue

'''
监听多个客户端。
将监听和处理放到不同的线程进行处理。
将监听放入主线程，将处理放进子线程。
该服务器功能为接收到客户端的数据后加密并返回给客户端。
'''
t = threading.Thread(target=handle)
if __name__ == '__main__':
    t.start()
    print(r'我在%s线程中' % threading.current_thread().name)
    print('waiting for connecting ...')
    while True:
        CliSock,addr = SerSock.accept()
        print('connected from:',addr)
        socks.append(CliSock)

