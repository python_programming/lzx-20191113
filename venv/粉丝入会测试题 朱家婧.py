'''
2020年3月11日
xx粉丝协会入会测试：
(设计者：朱家婧)
'''
k=0
print("这是xx粉丝协会入会测试，测试要求如下：")
print("本测试共有三道题，答错一题则必须重来，测试共有3次作答机会")
for i in range(3):
    print("第一题：xx的生日是（ ）")
    print("A.3月6日  B.3月10日  C.5月6日  D.3月5日")
    choose1 = input()
    if  'D'==choose1:
        print("正确")
        print("第二题：xx没有饰演过哪个角色（ ）")
        print("A.fcfh  B.lcl  C.xb  D.wl")
        choose2 = input()
        if 'C' == choose2:
            print("正确")
            print("第三题：xx的应援色是（ ）")
            print("A.紫色  B.红色  C.蓝色  D.灰色")
            choose3 = input()
            if 'A' == choose3:
                print("正确")
                k=1
                break
            else:
                print("错误")
                continue
        else:
            print("错误")
            continue
    else:
        print("错误")


if 0==k:
    print("很抱歉，您不能加入协会。")
else:
    print("恭喜,您已成为协会一员！")