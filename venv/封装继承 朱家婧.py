class People:
    __head="一个头"
    __arm="两只手臂"
    __leg="两条腿"
    def __init__(self):
        print(People.__head,People.__arm,People.__leg)
    def deff(self,race):
        self.race=race
class Asian(People):
    race="亚洲人"
    def characteristics(self,hair,face):
        self.hair=hair
        self.face=face
        print("人种是：", self.race)
        print("头发是", self.hair)
        print("肤色是", self.face)
class European(People):
    race = "欧洲人"
    def characteristics(self, hair, face):
        self.hair = hair
        self.face = face
        print("人种是：", self.race)
        print("头发是", self.hair)
        print("肤色是", self.face)
asian=Asian()
asian.characteristics("黑色的","黄色")
european=European()
european.characteristics("金色的","白色")