# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：学习足迹记录
时间：2020年3月18日
'''

fundamental_compulsory = ["思修道德修养与法律基础","中国近代史纲要","大学物理","大学英语","程序设计基础","体育基础",
                          "军事理论","大学语文","形势与政策","大学生心理健康"]
major_compulsory = ["信息与计算科学导论","数学分析","高等代数","解析几何"]
fundamental_optional = ["社会心理学","大学生数学思维训练","Python程序设计"]
major_optional = ["无"]

#遍历
print("林紫欣同学已经学过及正在学习的课程有：\n")

print("=======基础必修课=======")
for course in fundamental_compulsory:
    print(course, end=' ')

print("\n=======基础选修课=======")
for course in fundamental_optional:
    print(course, end=' ')

print("\n=======专业必修课=======")
for course in major_compulsory:
    print(course, end=' ')

print("\n=======专业选修课=======")
for course in major_optional:
    print(course, end=' ')

#增加、删除、修改
print("\n\n信息是否需要调整？")
print('''  如需添加课程请输入数字1
  如需删除课程请输入数字2
  如需修改课程请输入数字3
  不需要调整请输入数字0''')
t = int(input("\n请输入相关数字："))
while t==1 or t==2 or t==3:
    if t == 1:
        name = input("请输入想要添加的课程的所属类目（基础必修课，基础选修课，专业必修课，专业选修课）：")
        if name == '基础必修课':
            temp = input("请输入添加的课程名：")
            fundamental_compulsory.append(temp)
        elif name == '基础选修课':
            temp = input("请输入添加的课程名：")
            fundamental_optional.append(temp)
        elif name == '专业必修课':
            temp = input("请输入添加的课程名：")
            major_compulsory.append(temp)
        elif name == '专业选修课':
            temp = input("请输入添加的课程名：")
            major_optional.append(temp)

    if t == 2:
        name = input("请输入想要删除的课程的所属类目（基础必修课，基础选修课，专业必修课，专业选修课）：")
        if name == '基础必修课':
            temp = input("请输入删除的课程名：")
            fundamental_compulsory.remove(temp)
        elif name == '基础选修课':
            temp = input("请输入删除的课程名：")
            fundamental_optional.remove(temp)
        elif name == '专业必修课':
            temp = input("请输入删除的课程名：")
            major_compulsory.remove(temp)
        elif name == '专业选修课':
            temp = input("请输入删除的课程名：")
            major_optional.remove(temp)

    if t == 3:
        name = input("请输入想要修改的课程的所属类目（基础必修课，基础选修课，专业必修课，专业选修课）：")
        if name == '基础必修课':
            number = int(input("请输入需要修改的课程所在号位（第几个）："))
            temp = input("请输入修改后的课程名称：")
            fundamental_compulsory[number-1] = temp
        elif name == '基础选修课':
            number = int(input("请输入需要修改的课程所在号位（第几个）："))
            temp = input("请输入修改后的课程名称：")
            fundamental_optional[number-1] = temp
        elif name == '专业必修课':
            number = int(input("请输入需要修改的课程所在号位（第几个）："))
            temp = input("请输入修改后的课程名称：")
            major_compulsory[number-1] = temp
        elif name == '专业选修课':
            number = int(input("请输入需要修改的课程所在号位（第几个）："))
            temp = input("请输入修改后的课程名称：")
            major_optional[number-1] = temp

    print("\n是否需要进行再次调整？")
    print('''  如需添加课程请输入数字1
  如需删除课程请输入数字2
  如需修改课程请输入数字3
  不需要调整请输入数字0''')
    t = int(input("\n请输入相关数字："))

print("\n请查核更改后的信息：")
print("\n林紫欣同学已经学过及正在学习的课程有：\n")

print("=======基础必修课=======")
for course in fundamental_compulsory:
    print(course, end=' ')

print("\n=======基础选修课=======")
for course in fundamental_optional:
    print(course, end=' ')

print("\n=======专业必修课=======")
for course in major_compulsory:
    print(course, end=' ')

print("\n=======专业选修课=======")
for course in major_optional:
    print(course, end=' ')