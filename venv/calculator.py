# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：计算器程序设计
时间：2020年4月12日
'''
import math
def ari():
    '''四则运算'''
    print("1.二元加法运算\n2.二元减法运算\n3.二元乘法运算\n4.二元除法运算\n")
    temp = 233
    while temp != 1 and temp != 2 and temp != 3 and temp != 4:
        temp = int(input("请依照所要使用的功能输入相应的数字序号："))
        if temp == 1 or temp == 2 or temp == 3:
            x = float(input("请输入第一个操作数："))
            y = float(input("请输入第二个操作数："))
            if temp == 1:
                print(x + y)
            elif temp == 2:
                print(x - y)
            else:
                print(x * y)
        elif temp == 4:
            x = float(input("请输入被除数："))
            y = float(input("请输入除数："))
            while y == 0:
                print("输入错误！除数不能为0！")
                y = float(input("请重新输入除数："))
            print(x / y)
        else:
            print("相应功能数字序号输入有误！")
            print("1.二元加法运算\n2.二元减法运算\n3.二元乘法运算\n4.二元除法运算\n")
def mol():
    '''取模运算'''
    x = int(input("请输入整数被除数："))
    y = int(input("请输入整数除数："))
    while y == 0:
        print("输入错误！除数不能为0！")
        y = int(input("请重新输入整数除数："))
    print(x % y)

def act():
    '''求简单三角函数'''
    print("\n1.求正弦\n2.求余弦\n3.求正切\n4.求反正弦\n5.求反余弦\n6.求反正切")
    temp = 233
    while temp != '1' and temp != '2' and temp != '3' and temp != '4' and temp != '5' and temp != '6':
        temp = input("请依照所要使用的功能输入相应的数字序号：")
        if temp == '1' or temp == '2' or temp == '3':
            x = float(input("请以角度制输入角度："))
            x = x / 180 * math.pi
            if temp == '1':
                print('%.2f' %math.sin(x))
            elif temp == '2':
                print('%.2f' %math.cos(x))
            elif temp == '3':
                print('%.2f' %math.tan(x))
        elif temp == '4' or temp == '5' or temp == '6':
            x = float(input("请输入相应数值，答案将以角度制呈现:"))
            if temp == '4':
                if -1 <= x and x <= 1:
                    print('%.2f' % math.degrees(math.asin(x)))
                else:
                    print("无解！")
            elif temp == '5':
                x = float(input("请输入相应数值（-1≤x≤1）:"))
                if -1 <= x and x <= 1:
                    print('%.2f' % math.degrees(math.acos(x)))
                else:
                    print("无解！")
            elif temp == '6':
                x = float(input("请输入相应数值:"))
                print('%.2f' % math.degrees(math.atan(x)))
        else:
            print("相应功能数字序号输入有误！")

def nqo():
    '''求一个数的n次方'''
    a = float(input("请输入底数："))
    while a == 0:
        print("输入错误！底数不能为0！")
        a = int(input("请重新输入底数："))
    n = float(input("请输入指数："))
    print(math.pow(a,n))

def fac():
    '''求一个数的阶乘 '''
    n = int(input("请输入一个整数："))
    print(math.factorial(n))

def qua():
    '''解一元二次方程'''
    print('*' * 7 + "解一元二次方程 ax^2 + bx + c = 0" + '*' * 7)
    a = float(input("请输入a："))
    b = float(input("请输入b："))
    c = float(input("请输入c："))
    d = b*b - 4*a*c
    if d>=0:
        x1 = (-b+math.sqrt(d)) / (2*a)
        x2 = (-b-math.sqrt(d)) / (2*a)
        if d==0:
            return "有唯一的解：X = "+str(x1)
        else:
            return "X1 = "+str(x1)+'\t'+"X2 = "+str(x2)
    else:
        x1 = str(-b/(2*a)) + '+' + str(math.sqrt(-d)/(2*a)) + 'i'
        x2 = str(-b/(2*a)) + '-' + str(math.sqrt(-d)/(2*a)) + 'i'
        return "X1 = "+x1+'\t'+"X2 = "+x2

def sta():
    '''统计运算'''
    x = int(input("输入数据个数："))
    y = []
    print("请输入数据：")
    for i in range(x):
        temp = [float(input())]
        y += temp
    print("\n1.求和\n2.求平均值\n3.求极差\n4.求方差\n5.求标准差")
    temp = 233
    while temp != '1' and temp != '2' and temp != '3' and temp != '4' and temp != '5':
        temp = input("请依照所要使用的功能输入相应的数字序号：")
        s = 0
        for i in range(x):
            s = s + y[i]
        var = 0
        for i in range(x):
            var = var + (s / x - y[i]) ** 2
        var = var / x
        if temp == '1':
            print(s)
        elif temp == '2':
            print(s / x)
        elif temp == '3':
            print(max(y) - min(y))
        elif temp == '4':
            print(var)
        elif temp == '5':
            print(math.sqrt(var))
        else:
            print("相应功能数字序号输入有误！")



#计算机功能框架
print('*'*5+"欢迎使用计算器！"+'*'*5)
print("本计算器提供的功能有：")
print("1.简单二元四则运算")
print("2.取模运算")
print("3.求简单三角函数")
print("4.求一个数的n次方")
print("5.求一个数的阶乘")
print("6.解一元二次方程")
print("7.统计运算")
print("-1.退出程序")
t = 233

while t!='-1':
    t = input("\n请依照所要使用的功能输入相应的数字序号：")
    if t=='1':
        ari()
    elif t=='2':
        mol()
    elif t=='3':
        act()
    elif t=='4':
        nqo()
    elif t=='5':
        fac()
    elif t=='6':
        print(qua())
    elif t=='7':
        sta()
    elif t=='-1':
        exit(0)
    else:
        print("相应功能数字序号输入有误！")

    print("\n是否再次计算？")
    print("本计算器提供的功能有：")
    print("1.二元四则运算")
    print("2.取模运算")
    print("3.求简单三角函数")
    print("4.求一个数的n次方")
    print("5.求一个数的阶乘")
    print("6.解一元二次方程")
    print("7.统计运算")
    print("-1.退出程序")