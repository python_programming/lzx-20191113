# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：星座
时间：2020年4月1日
'''
def deter_con(month,date):
    constellations =  ('摩羯座','水瓶座','双鱼座','白羊座','金牛座','双子座','巨蟹座',
                       '狮子座','处女座','天秤座','天蝎座','射手座','摩羯座')
    dates = (20,19,21,20,21,22,23,23,23,24,23,22)
    if date < dates[month-1]:
        return constellations[month-1]
    else:
        return constellations[month]

month = input("请输入月份（例如：7）：")
date = input("请输入日期（例如：13）：")
constellation = deter_con(int(month),int(date))
print(month+"月"+date+"日所属的星座是"+constellation)