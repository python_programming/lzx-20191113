# 第一题
'''
作者：林紫欣
文件名称：Python语言基础实践
时间：2020年3月4日
'''

# 第二题
# 加密过程
x = int(input("Please enter the number encoded in plaintext:"))
y = (5*x+8) % 26
print("The encryption result is:",y,"\n")
# 解密过程
t = int(input("Please enter cipher:"))
r = 21*(t-8) % 26
print("The decryption result is:",r,"\n")

# 第三题
a = float(input("Please enter the value of a:"))
n = int(input("Please enter the value of n:"))
q = float(input("Please enter the value of q（q != 1）:"))
s = a*((q**n)-1) / (q-1)
print("Sn =",s,"\n")