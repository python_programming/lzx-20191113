# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：Python之Socket技术及安全编程技术【后续加分篇-界面】——TCP服务器端
时间：2020年5月7日
'''
import tkinter
from tkinter import messagebox
import socket

# 文件接收主操作
def TakeIn():
    server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server.bind(('127.0.0.1',8001))
    server.listen()
    while True:
        conn,address = server.accept()
        while True:
            with open(FilePath,'ab') as file:
                data = conn.recv(1024)
                if data == b'quit':
                    break
                file.write(data)
            conn.sendall('success'.encode())
        print("文件接收完成")
        print("文件内容为：")
        file = open(FilePath,'r',encoding='utf-8')
        print(file.read())
    server.close()

# 弹窗设置
class MyCollectApp(tkinter.Toplevel):
    def __init__(self):
        super().__init__()
        self.title('文件信息')
        self.setupUI()

    def setupUI(self):
        row1 = tkinter.Frame(self)
        row1.pack(fill="x")
        l1 = tkinter.Label(row1, text="文件路径：", height=2, width=10)
        l1.pack(side=tkinter.LEFT)
        self.xls_text = tkinter.StringVar()
        tkinter.Entry(row1, textvariable=self.xls_text).pack(side=tkinter.RIGHT)

        row2 = tkinter.Frame(self)
        row2.pack(fill="x")
        tkinter.Button(row2, text="点击确认", command=self.on_click).pack(side=tkinter.RIGHT)

    def on_click(self):
        global FilePath
        FilePath = self.xls_text.get().lstrip()
        if len(FilePath) == 0:
            messagebox.showwarning(title='系统提示', message='请输入文件保存路径!')
            return False
        self.quit()
        self.destroy()
        # 调用接收函数
        TakeIn()
        print("文件接收成功！")

# 主函数
if __name__ == '__main__':
    window = tkinter.messagebox.askyesno(title='系统提示', message='是否接收文件？')  # 返回'True','False'
    if window:
        app = MyCollectApp()
        app.mainloop()
    else:
        print('无操作实现')
