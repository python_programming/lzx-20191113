# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：自信心状况测量
时间：2020年3月11日
'''

print("想知道自己的自信心状况吗？那就来测一测吧！")
grade = 0
if int(input("我想测试（输入数字1）\n我不想测试（输入数字0）\n你的选择是：")):
    print("\n注意：以下试题，选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n1.当老师在班级里提出某个问题时，你会采取哪一种态度?
  A.马上举手表明自己的意见
  B.除非老师叫我起来回答，否则保持沉默
  C.等大家发完言后再发表自己的看法""")
    while t!=1 and t!=2 and t!=3 :
        t = int(input("你的答案是："))
        if t == 1:
            grade += 5
        elif t == 2:
            grade += 0
        elif t == 3:
            grade += 5
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n2.如果老师对你进行不适当的批评，你将采取哪一种态度?
  A.马上全力为自己辩护，且情绪激动
  B.冷静地、理智地表明自己的看法
  C.不声不响也不辩护，但记在心里""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 1
        elif t == 2:
            grade += 5
        elif t == 3:
            grade += 0
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n3.全校举行演讲比赛，老师和同学都推荐你去，你将如何对待?
  A.以种种借口推脱，坚决不去
  B.同意去，但演讲什么要老师和同学一起出主意
  C.不马上答应，等考虑后再做答复""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 0
        elif t == 2:
            grade += 5
        elif t == 3:
            grade += 1
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n4.当你的好友在同学面前提出你认为不好的要求，如借作业本抄，你怎么办?
  A.表面上答应，但过一会找个借口不给他
  B.给他讲抄作业的害处，帮他弄懂难点，让他自己完成
  C.听听其他同学的意见，再决定是否给他""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 0
        elif t == 2:
            grade += 5
        elif t == 3:
            grade += 1
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n5.当你去参加校学生会举行的座谈会时，你首先做的是:
  A.找认识的同学，坐在一起交谈
  B.与旁边不认识的同学相互认识起来，并进行交谈
  C一个人坐那里，不言语，看其他同学谈论""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 1
        elif t == 2:
            grade += 5
        elif t == 3:
            grade += 0
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")


    t = 2333
    print("""\n6.如果你被同学选为班长，你怎么办?
  A.勇敢地接受，并负责地把班级工作做好
  B.同意试试，但随时准备退出
  C.要求同学们支持、配合你的工作""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 5
        elif t == 2:
            grade += 0
        elif t == 3:
            grade += 1
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n7.如果老师要求你做一件关系到你声誉的工作时，你怎么办?
  A.请老师讲一讲做好这一工作的关键是什么
  B.明确表示做好这作的要点
  C.勉强接受,但也可能就打退堂鼓""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 1
        elif t == 2:
            grade += 5
        elif t == 3:
            grade += 0
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n8.如果老师上课一个地方讲错了，你怎么办?
  A.巧妙地向老师提出问题，指出讲错的地方
  B.借回答问题来纠正老师讲课中的差错
  C.下课后再向老师提出""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 5
        elif t == 2:
            grade += 1
        elif t == 3:
            grade += 0
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n9.如果让你当班长，在挑选其他班委会成员时，你将选择哪一种人？
  A.学习好，但只顾自己的人
  B.小有缺点，但乐意为集体服务的人
  C.学习好，大事做不来，小事尚能做的人""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 0
        elif t == 2:
            grade += 5
        elif t == 3:
            grade += 1
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    t = 2333
    print("""\n10.如果你来当老师，你将如何对待学生?
  A.想同学所想，通情达理
  B.模仿老师的办法
  C.有些照老师的办法，有些根据自已的体验""")
    while t != 1 and t != 2 and t != 3:
        t = int(input("你的答案是："))
        if t == 1:
            grade += 5
        elif t == 2:
            grade += 0
        elif t == 3:
            grade += 1
        else:
            print("\n输入格式无效，请重新输入。\n选择A请输入数字1，选择B请输入数字2，选择C请输入数字3。")

    print("\n答题已结束，是否要查看测试结果呢？")
    print("1.马上查看！（请输入数字1）")
    print("2.查什么查，我已猜到结果。（随意输入任意一个数字）")
    t = int(input("你的选择是："))

    if t == 1:
        if 50 > grade >= 40:
            print("\n你是一个很有自信心的人。你敢于自告奋勇地做事，但必须小心，讲究工作技巧。")
            print("\n测试结束。")
        elif 40 > grade >= 28:
            print("\n你有较强的自信心，并在多数情况下能应付自如，但在通往直前时，要保持谨慎。")
            print("\n测试结束。")
        elif 28 > grade >= 11:
            print("\n你办事缩手缩脚，总怕出差错，你应设法肯定自我，增强自信。")
            print("\n测试结束。")
        else:
            print("\n你给人的印象似乎不存在似的，应努力改变这种情况，须知自信是成功的一半。")
            print("\n测试结束。")
    else:
        print("\n答案咱们心照不宣，测试结束。")