# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：Python数据库实践——MySQL
时间：2020年4月29日
'''
import pymysql
#创建连接对象
db = pymysql.connect(host = 'localhost',user = 'root',database = '20191113',password = 'Lzx%1314mylove')
cursor = db.cursor()
#创建表格
sql = """
create table if not exists books (
  id int(10) NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  price decimal(10,2) DEFAULT NULL,
  publish_time date DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
"""
cursor.execute(sql)
#插入
data = [('191101','The Great Gatsby','13.14','1925-06-30'),
        ('191102','The Little Prince','13.15','1942-01-13')]
sql = 'insert into books (id,name,price,publish_time) value (%s,%s,%s,%s)'
cursor.executemany(sql,data)
#查询
cursor.execute('select * from books')
print("\n插入后的答案是：")
print(cursor.fetchall())
#修改
cursor.execute('update books set price = 13.14 where id = 191102')
cursor.execute('select * from books')
print("\n修改后的答案是：")
print(cursor.fetchall())
#删除
cursor.execute('delete from books where id = 191102')
cursor.execute('select * from books')
print("\n删除后的答案是：")
print(cursor.fetchall())
print("\n")
#关闭
cursor.close()
db.close()