# -*- coding: utf-8 -*-
def fun_BMI(*person):
    '''功能：根据身高和体重计算BMI指数
    person:可变参数：需要传递带3个值的列表，
    分别为：姓名、身高（单位：米）、体重（单位：千克）'''
    for list_item in person:
        for item in list_item:
            person = item[0]
            height = item[1]
            weight = item[2]
            print(person + "的身高：" + str(height) + "米\t体重：" + str(weight) + "千克")

            BMI = weight / (height*height)  # 计算BMI指数
            print(person+"的BMI指数：",BMI)
            if BMI < 18.5:
                print(person+"的体重过轻 ~@_@~")
            if BMI >= 18.5 and BMI <= 24.9:
                print(person+"的体重在正常范围，注意保持哦 (^-^)/")
            if BMI >= 24.9 and BMI < 29.9:
                print(person+"的体重过重 ~@_@~")
            if BMI >= 29.9:
                print(person+"的体重属于肥胖行列 ~#_#~")

list_w = [['20191113',1.62,50],['20191108',1.58,48],['20191122',1.62,54]]
list_m = [['20191101',1.72,52],['20191102',1.75,60],['20191111',1.79,70]]
fun_BMI(list_w,list_m)
#print(fun_BMI.__defaults__)  # 查看默认值
