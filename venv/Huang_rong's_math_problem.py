print("今有物不知其数，三三数之剩二，五五数之剩三，七七数之剩二，问几何？\n")
none = int(input("请选择模式：\n1.回答问题请输入数字1\n2.查看参考答案请输入数字0\n您选择的模式是："))
if none == 1:
    while 1:
        number = int(input("\n请输入认为符合条件的数："))
        if number%3==2 and number%5==3 and number%7==2 :
            print("成功回答本问题！")
            break
        else:
            t = int(input("很遗憾，回答错误。如果想继续回答，请输入数字1；想查看答案，请输入数字0\n您的选择是："))
            if t == 1:
                continue
            else:
                print("\n答案为：23")
else:
    for i in range(100):
        if i%3==2 and i%5==3 and i%7==2 :
            print("\n答案为：",i)
            break