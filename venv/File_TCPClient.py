# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：Python之Socket技术及安全编程技术【后续加分篇-文件】——TCP客户端
时间：2020年5月6日
'''
import socket

def TakeIn():
    server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server.bind(('127.0.0.1',8001))
    server.listen()
    while True:
        conn,address = server.accept()
        while True:
            with open(filename,'ab') as file:
                data = conn.recv(1024)
                if data == b'quit':
                    break
                file.write(data)
            conn.sendall('success'.encode())
        print("文件接收完成")
        print("文件内容为：")
        file = open(filename,'r',encoding='utf-8')
        print(file.read())
    server.close()

def SendOut():
    client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    client.connect(('127.0.0.1',8001))
    with open(filename,'rb') as file:
        for i in file:
            client.send(i)
            data = client.recv(1024)
            if data != b'success':
                break
    client.send('quit'.encode())

temp = " "
while temp!="是" and temp!="否":
    print("是否传输文件？")
    temp = input("请输入 是/否 ：")
    if temp == "是":
        filename = input("请输入所要传输的文件名：")
        SendOut()
    elif temp == "否":
        filename = input("所接收的文件将命名为：")
        TakeIn()



