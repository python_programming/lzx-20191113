# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：豆瓣十大高分电影排行榜
时间：2020年3月11日
'''
movies = [('《The Shawshank Redemption》',9.7),
         ('《霸王别姬》',9.6),
         ('《Forrest Gump》',9.5),
         ('《Léon》',9.4),
         ('《La vita è bella》',9.5),
         ('《Titanic》',9.4),
         ('《千与千寻 千と千尋の神隠し》',9.3),
         ('《Schindler\'s List》',9.5),
         ('《Inception》',9.3),
         ('《Hachi: A Dog\'s Tale》',9.4)]
movies = sorted(movies,key=lambda s: s[1],reverse=True)
print("豆瓣十大高分电影排行榜：")
for i in range(10):
    print("%s    评分：%.1f" % (movies[i][0],movies[i][1]))

#增加、修改、删除
print("\n\n电影信息是否需要调整？")
print('''  如需添加电影请输入数字1
  如需删除电影请输入数字2
  不需要调整请输入数字0''')
t = int(input("\n请输入相关数字："))
x = 0
while t==1 or t==2:
    if t == 1:
        temp = input("请输入要添加的电影名称（请加入书名号）：")
        score = float(input("请输入这部电影的评分："))
        movie = (temp,score)
        movies.append(movie)
        x += 1
    if t == 2:
        temp = int(input("请输入要删除的电影排名："))
        del movies[temp]
        x -= 1
    print("\n电影信息是否需要再次进行调整？")
    print('''  如需添加电影请输入数字1
  如需删除电影请输入数字2
  不需要调整请输入数字0''')
    t = int(input("\n请输入相关数字："))

print("\n请核查信息：")
movies = sorted(movies,key=lambda s: s[1],reverse=True)
print("\n豆瓣十大高分电影排行榜：")
for i in range(10+x):
    print("%s    评分：%.1f" % (movies[i][0],movies[i][1]))