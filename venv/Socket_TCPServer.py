# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：Python之Socket技术及安全编程技术——TCP服务器端
时间：2020年5月6日
'''
import socket
# 创建套接字，绑定套接字到本地IP与端口
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind(('127.0.0.1',8001))
# 开始监听连接
s.listen()
# 进入循环，不断接受客户端的连接请求
conn,address = s.accept()
# 接收传来的数据，并发送给对方数据
data = conn.recv(1024)
print(data.decode())
conn.sendall(("服务器已经接收到了数据内容："+str(data)).encode())
# 传输完毕后，关闭套接字
s.close()