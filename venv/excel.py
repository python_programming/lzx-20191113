# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：使用xlwt模块操作Excel
时间：2020年4月22日
'''
import xlwt,random
workbook = xlwt.Workbook()
worksheet = workbook.add_sheet('My Worksheet')

headings = ['学号','民族']
for i in range(0,len(headings)):
    worksheet.write(0,i,headings[i])
for i in range (1,14):
    for j in range(0,len(headings)):
        number = str(random.randint(20191101,20191133))
        contents = [number,random.choices("汉""满""藏""苗""彝")]
        worksheet.write(i,j,contents[j])
workbook.save('1911.xls')