# -*- coding: utf-8 -*-
'''
作者：20191113 林紫欣
文件名称：文件操作实践
时间：2020年4月22日
'''
import  os
#任务1
path = r"D:\python\1911"
#os.mkdir(path)
#任务2
path = r"D:\python\1911\191113"
file = open(path,"a+")
#任务3
file.write("学号：20191113\n姓名：林紫欣\n")
file.write("课程名称：Python程序设计\n成绩：满分\n\n")
#任务4
file.write("学号：20191108\n姓名：朱家婧\n")
file.write("课程名称：Python程序设计\n成绩：满分\n\n")
#任务5
file = open(path,"r")
print(file.read())
#任务6
file.close()